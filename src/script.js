const {
    constants: { AddressZero },
} = require("ethers");
const axios = require("axios");

async function getGasPrice(
    sourceChain,
    destinationChain,
    tokenAddress,
    tokenSymbol
) {

    const api_url = "https://devnet.api.gmp.axelarscan.io";

    const requester = axios.create({ baseURL: api_url });
    const params = {
        method: "getGasPrice",
        destinationChain: destinationChain,
        sourceChain: sourceChain,
    };

    // set gas token address to params
    if (tokenAddress != AddressZero) {
        params.sourceTokenAddress = tokenAddress;
    } else {
        params.sourceTokenSymbol = tokenSymbol;
    }

    // send request
    const response = await requester.get("/", { params }).catch((error) => {
        return { data: { error } };
    });
    const result = response.data.result;

    console.log("result2");
    console.log(result);

    const dest = result.destination_native_token;
    const destPrice = 1e18 * dest.gas_price * dest.token_price.usd;
    return destPrice / result.source_token.token_price.usd;
}

(async () => {
    const result = await getGasPrice("Ethereum", "Polygon", "0xc778417e063141139fce010982780140aa0cd5ab", 'WETH');
    //const result = await getGasPrice("Polygon", "Ethereum", "0xA6FA4fB5f76172d178d61B04b0ecd319C5d1C0aa", 'WETH');

    console.log(result);
})();
