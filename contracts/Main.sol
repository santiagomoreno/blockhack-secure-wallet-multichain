// SPDX-License-Identifier: MIT
pragma solidity 0.8.9;

import {IAxelarExecutable} from "@axelar-network/axelar-cgp-solidity/src/interfaces/IAxelarExecutable.sol";
import {IAxelarGasReceiver} from "@axelar-network/axelar-cgp-solidity/src/interfaces/IAxelarGasReceiver.sol";
import "@openzeppelin/contracts/access/Ownable.sol";

contract Main is IAxelarExecutable, Ownable {
    mapping(uint256 => mapping(address => address[])) public tokens; // sateliteId -> (fundsOwner -> tokenAddresses)
    mapping(address => address[]) public heirs;
    mapping(address => mapping(address => bool)) public heirVotes; //heirAddress -> (fundsOwner -> bool)

    mapping(address => uint64) public ownerTimelock;
    mapping(address => uint64) public timeWStarted;
    mapping(address => uint8) public votesRequired;
    Satelite[] public satelites;
    IAxelarGasReceiver public gasReceiver;

    struct Satelite {
        string contractAddress;
        string destinationChain;
    }

    constructor(address gateway_, IAxelarGasReceiver gasReceiver_)
        IAxelarExecutable(gateway_)
    {
        gasReceiver = gasReceiver_;
    }

    //adders and deleters
    function addSatelite(Satelite memory _satelite) external onlyOwner {
        satelites.push(_satelite);
    }

    function deleteSatelite(uint256 index) external onlyOwner {
        require(index < satelites.length, "incorrect index");
        uint256 length = satelites.length - 1;
        satelites[index] = satelites[length];
        satelites.pop();
    }

    function addTokensToSatelite(uint256 sateliteId, address[] memory _tokens)
        external
    {
        require(sateliteId < satelites.length, "incorrect index");
        tokens[sateliteId][msg.sender] = _tokens;
    }

    function deleteTokensToSatelite(uint256 sateliteId) external {
        require(sateliteId < satelites.length, "incorrect index");
        delete tokens[sateliteId][msg.sender];
    }

    //mutable functions
    function secureFunds(
        address[] memory _heirs,
        uint64 _timelock,
        uint8 _votesRequired
    ) external returns (bool) {
        require(_heirs.length != 0, "There is no heirs");
        heirs[msg.sender] = _heirs;
        ownerTimelock[msg.sender] = _timelock;
        votesRequired[msg.sender] = _votesRequired;

        emit InsuredFunds(_heirs, _votesRequired);
        return true;
    }

    function setVote(address _owner, bool _vote) external {
        require(isHeir(_owner), "not an heir");
        heirVotes[msg.sender][_owner] = _vote;
        if (_lost(_owner)) {
            timeWStarted[_owner] = uint64(block.timestamp);
        }
    }

    function veto() external {
        timeWStarted[msg.sender] = 0;
    }

    function _lost(address _owner) private view returns (bool) {
        uint8 _votes;
        address[] memory _heirs = heirs[_owner];
        for (uint256 i; i < _heirs.length; ++i) {
            if (heirVotes[_heirs[i]][_owner]) {
                ++_votes;
            }
        }
        if (_votes >= votesRequired[_owner]) {
            return true;
        } else return false;
    }

    function isHeir(address user) private view returns (bool) {
        for (uint256 i; i < heirs[user].length; ++i) {
            if (heirs[user][i] == msg.sender) {
                return true;
            }
        }
        return false;
    }

    function initializeWithdrawal(address _owner) external payable {
        require(timeWStarted[_owner] != 0, "withdraw not started");
        require(block.timestamp > ownerTimelock[_owner] + timeWStarted[_owner], "not yet");

        for (uint256 i; i < satelites.length; ++i) {
            if (
                keccak256(abi.encodePacked(satelites[i].contractAddress)) !=
                keccak256(abi.encodePacked("")) && tokens[i][_owner].length != 0 
            ) {
                string memory destinationChain = satelites[i].destinationChain;
                string memory contractAddress = satelites[i].contractAddress;

                require(msg.value > 0, "should pay for gas");
                bytes memory payload = abi.encode(
                    _owner,
                    heirs[_owner],
                    tokens[i][msg.sender]
                );
                gasReceiver.payNativeGasForContractCall{value: msg.value}(
                    address(this),
                    destinationChain,
                    contractAddress,
                    payload,
                    msg.sender
                );
                gateway.callContract(
                    destinationChain,
                    contractAddress,
                    payload
                );
            }
        }
        emit InitializeWithdrawal(_owner);
    }

    event InsuredFunds(address[] _heirs, uint8 _votesRequired);
    event InitializeWithdrawal(address _owner);
}
