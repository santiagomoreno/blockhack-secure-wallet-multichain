// SPDX-License-Identifier: MIT
pragma solidity 0.8.9;


interface IAxelarGateway {
    /**********\
    |* Events  *|
    \**********/

    event ContractCall(
        address indexed sender,
        string destinationChain,
        string destinationContractAddress,
        bytes32 indexed payloadHash,
        bytes payload
    );

    /********************\
    |* Public Functions *|
    \********************/

    function callContract(
        string calldata destinationChain,
        string calldata contractAddress,
        bytes calldata payload
    ) external;
}

contract AxelarGatewayMock is IAxelarGateway{

    function callContract(
        string calldata destinationChain,
        string calldata destinationContractAddress,
        bytes calldata payload
    ) external override{
    }

}