// SPDX-License-Identifier: MIT
pragma solidity 0.8.9;

interface IAxelarGasReceiver {
    /********************\
    |* Public Functions *|
    \********************/

    // This is called on the source chain before calling the gateway to execute a remote contract.
    function payNativeGasForContractCall(
        address sender,
        string calldata destinationChain,
        string calldata destinationAddress,
        bytes calldata payload,
        address refundAddress
    ) external payable;
}

contract AxelarGasReceiverMock is IAxelarGasReceiver{

    function payNativeGasForContractCall(
        address sender,
        string calldata destinationChain,
        string calldata destinationAddress,
        bytes calldata payload,
        address refundAddress
    ) external payable{
    }

}