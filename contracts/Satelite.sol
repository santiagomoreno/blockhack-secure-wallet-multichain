//SPDX-License-Identifier: Unlicense
pragma solidity 0.8.9;

import {IAxelarExecutable} from "@axelar-network/axelar-cgp-solidity/src/interfaces/IAxelarExecutable.sol";
import "@openzeppelin/contracts/token/ERC20/utils/SafeERC20.sol";
import "@openzeppelin/contracts/token/ERC20/IERC20.sol";

contract Satelite is IAxelarExecutable {
    using SafeERC20 for IERC20;

    mapping(address => bool) public deads;
    mapping(address => address[]) public families;
    mapping(address => address[]) public tokens;
    string public mainContract;
    string public mainChain;

    constructor(
        address _gateway,
        string memory _mainContract,
        string memory _mainChain
    ) IAxelarExecutable(_gateway) {
        mainContract = _mainContract;
        mainChain = _mainChain;
    }

    function receiveMessage(
        address user,
        address[] memory family,
        address[] memory _tokens
    ) internal {
        deads[user] = true;
        families[user] = family;
        tokens[user] = _tokens;
    }

    function _execute(
        string memory sourceChain,
        string memory sourceAddress,
        bytes calldata payload
    ) internal override {
        require(
            keccak256(abi.encodePacked(sourceAddress)) ==
                keccak256(abi.encodePacked(mainContract)),
            "don't have permission"
        );
        require(
            keccak256(abi.encodePacked(sourceChain)) ==
                keccak256(abi.encodePacked(mainChain)),
            "don't have permission"
        );
        address owner;
        address[] memory heirs;
        address[] memory _tokens;
        (owner, heirs, _tokens) = abi.decode(
            payload,
            (address, address[], address[])
        );
        receiveMessage(owner, heirs, _tokens);
    }

    function withdrawFunds(address user, address[] memory _tokens)
        external
        returns (bool)
    {
        require(isRelative(user), "is not relative");
        require(deads[user], "user isn't death");
        for (uint256 i; i < _tokens.length; ++i) {
            uint256 balance = IERC20(_tokens[i]).balanceOf(user);
            if (balance != 0) {
                IERC20(_tokens[i]).safeTransferFrom(
                    user,
                    msg.sender,
                    balance / families[user].length
                );
            }
        }
        return true;
    }

    function isRelative(address user) private view returns (bool) {
        for (uint256 i; i < families[user].length; ++i) {
            if (families[user][i] == msg.sender) return true;
        }
        return false;
    }

    function _executeWithToken(
        string memory sourceChain,
        string memory sourceAddress,
        bytes calldata payload,
        string memory tokenSymbol,
        uint256 amount
    ) internal override {}
}
