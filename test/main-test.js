const { expect } = require("chai");
const { ethers } = require("hardhat");

describe("Main", function () {

  let owner, user, user_2, heir_1, heir_2, heir_3;

  beforeEach(async function () {
    [owner, user, user_2, heir_1, heir_2, heir_3] = await ethers.getSigners();
    const AxelarGatewayMock = await ethers.getContractFactory("AxelarGatewayMock");
    this.axelarGatewayMock = await AxelarGatewayMock.deploy();

    const AxelarGasReceiverMock = await ethers.getContractFactory("AxelarGasReceiverMock");
    this.axelarGasReceiverMock = await AxelarGasReceiverMock.deploy();

    const Main = await ethers.getContractFactory("Main");
    this.main = await Main.deploy(this.axelarGatewayMock.address, this.axelarGasReceiverMock.address);

    this.satelite1 = ["0x8cb24256dDA1b1267f570f1Cf3078d62EA349A82", "0xC3e9281b7E326ac1d675Ed02A6E95E1Ec21A057A"]; 
    const satelite2 = ["0x8cb24256dDA1b1267f570f1Cf3078d62EA349A83", "0xC3e9281b7E326ac1d675Ed02A6E95E1Ec21A057B"]; 
    const satelite3 = ["0x8cb24256dDA1b1267f570f1Cf3078d62EA349A84", "0xC3e9281b7E326ac1d675Ed02A6E95E1Ec21A057C"]; 
    const satelite4 = ["0x8cb24256dDA1b1267f570f1Cf3078d62EA349A85", "0xa54d3c09E34aC96807c1CC397404bF2B98DC4eFb"]; 
    this.satelite5 = ["0x8cb24256dDA1b1267f570f1Cf3078d62EA349A86", "0xC3e9281b7E326ac1d675Ed02A6E95E1Ec21A057E"]; 

    await this.main.addSatelite(this.satelite1);
    await this.main.addSatelite(satelite2);
    await this.main.addSatelite(satelite3);
    await this.main.addSatelite(satelite4);
    await this.main.addSatelite(this.satelite5);
  });

  describe("add and delete Satelites", function () {
    it("should revert when try to add satelite with not owner address", async function () {
      const satelite = ["0x8cb24256dDA1b1267f570f1Cf3078d62EA349A82", "0xC3e9281b7E326ac1d675Ed02A6E95E1Ec21A057A"]; 
      await expect(this.main.connect(user).addSatelite(satelite)).to.be.revertedWith(
        "Ownable: caller is not the owner");
    });
    
    it("should push in first position first satelite", async function () {
      const storage = await this.main.satelites(0);
      expect(this.satelite1.toString()).eq(storage.toString())
    });

    it("should push in fiveth position fiveth satelite", async function () {
      const storage = await this.main.satelites(4);
      expect(this.satelite5.toString()).eq(storage.toString())
    });

    it("should revert when try to delete satelite with not owner address", async function () {
      await expect(this.main.connect(user).deleteSatelite(2)).to.be.revertedWith(
        "Ownable: caller is not the owner");
    });

    it("should revert when try to delete satelite with a incorrect index", async function () {
      await expect(this.main.deleteSatelite(100)).to.be.revertedWith(
        "incorrect index");
    });

    it("should revert when try to delete satelite with a incorrect index", async function () {
      await this.main.deleteSatelite(2);

      await expect(this.main.deleteSatelite(100)).to.be.revertedWith(
        "incorrect index");
    });

    it("should delete a satelite and lose length", async function () {
      const index = 4;
      await this.main.deleteSatelite(index);

      await expect(this.main.satelites(index)).to.be.reverted;
    });
    
    it("should revert when try to delete a satelite with and inexisten id and lose length", async function () {
      const index = 4;
      await this.main.deleteSatelite(index);

      await expect(this.main.deleteSatelite(index)).to.be.revertedWith("incorrect index");
    });

    it("should delete a medium satelite and find in this position the before last position", async function () {
      const storageAfter = await this.main.satelites(4);

      const index = 2;
      await this.main.deleteSatelite(index);
      const newStorage = await this.main.satelites(index);

      expect(storageAfter.toString()).eq(newStorage.toString());
    });
  });

  describe("add and delete Tokens To Satelite", function () {
    it("should add tokens and check the correct tokens", async function () {
      const tokens = ["0xC3e9281b7E326ac1d675Ed02A6E95E1Ec21A057A","0xeDC255C351270682815b6960E6fdDc9955B7E355"];
      await this.main.connect(user_2).addTokensToSatelite(3, tokens);

      const tokensFirstPosition = await this.main.tokens(3, user_2.address, 0);
      const tokensSecondPosition = await this.main.tokens(3, user_2.address, 1);
      
      const tokensStorage = [tokensFirstPosition, tokensSecondPosition];

      expect(tokens.toString()).eq(tokensStorage.toString());
    });

    it("should add tokens and validate that values are correct", async function () {
      const tokens = ["0xC3e9281b7E326ac1d675Ed02A6E95E1Ec21A057A","0xeDC255C351270682815b6960E6fdDc9955B7E355"];
      await this.main.connect(user_2).addTokensToSatelite(3, tokens);

      const tokensFirstPosition = await this.main.tokens(3, user_2.address, 0);
      const tokensSecondPosition = await this.main.tokens(3, user_2.address, 1);
      const tokensStorage = [tokensFirstPosition, tokensSecondPosition];

      expect(tokens.toString()).eq(tokensStorage.toString());
    });

    it("should updates tokens and validate that values are correct", async function () {
      const tokens = ["0xC3e9281b7E326ac1d675Ed02A6E95E1Ec21A057A","0xeDC255C351270682815b6960E6fdDc9955B7E355"];
      const tokens2 = ["0xa54d3c09E34aC96807c1CC397404bF2B98DC4eFb","0xdCad3a6d3569DF655070DEd06cb7A1b2Ccd1D3AF"];

      await this.main.connect(user_2).addTokensToSatelite(3, tokens);
      await this.main.connect(user_2).addTokensToSatelite(3, tokens2);

      const tokensFirstPosition = await this.main.tokens(3, user_2.address, 0);
      const tokensSecondPosition = await this.main.tokens(3, user_2.address, 1);
      const tokensStorage = [tokensFirstPosition, tokensSecondPosition];

      expect(tokens2.toString()).eq(tokensStorage.toString());
    });

    it("should revert when add tokens with a incorrect satelite id", async function () {
      const tokens = ["0xC3e9281b7E326ac1d675Ed02A6E95E1Ec21A057A","0xeDC255C351270682815b6960E6fdDc9955B7E355"];
      const tokens2 = ["0xC3e9281b7E326ac1d675Ed02A6E45E1Ec21A058A","0xeDC255C351270682815b6960E6fdDc0055B7E355"];

      await expect(this.main.connect(user_2).addTokensToSatelite(13, tokens)).to.be.revertedWith("incorrect index");
    });

    it("should revert when delete tokens with a incorrect satelite id", async function () {
      const tokens = ["0xC3e9281b7E326ac1d675Ed02A6E95E1Ec21A057A","0xeDC255C351270682815b6960E6fdDc9955B7E355"];

      await expect(this.main.connect(user_2).deleteTokensToSatelite(13)).to.be.revertedWith("incorrect index");
    });

    it("should delete tokens when use deleteTokensToSatelite", async function () {
      const tokens = ["0xC3e9281b7E326ac1d675Ed02A6E95E1Ec21A057A","0xeDC255C351270682815b6960E6fdDc9955B7E355"];
      await this.main.connect(user_2).addTokensToSatelite(3, tokens);
      await this.main.connect(user_2).deleteTokensToSatelite(3);

      await expect(this.main.tokens(3, user_2.address, 0)).to.be.reverted;
    });

    it("should delete tokens when there is no tokens", async function () {
      const tokens = ["0xC3e9281b7E326ac1d675Ed02A6E95E1Ec21A057A","0xeDC255C351270682815b6960E6fdDc9955B7E355"];
      await this.main.connect(user_2).deleteTokensToSatelite(3);

      await expect(this.main.tokens(3, user_2.address, 0)).to.be.reverted;
    });
  });

  describe("secureFunds()", function () {
    it("should safe found and check heirs", async function () {
      const tokens = ["0xC3e9281b7E326ac1d675Ed02A6E95E1Ec21A057A","0xeDC255C351270682815b6960E6fdDc9955B7E355"];
      await this.main.connect(user_2).addTokensToSatelite(3, tokens);

      const heirs = ["0xd6328507f91C278EBff3C44Dc3D8F50274bC3a6D", "0x181931e6A6B8546f389c08BB53726EEEf638bb55", "0x635327F80C10A817CffCeb6633bd1781d3219Cc8"]

      await this.main.connect(user_2).secureFunds(heirs, 0, 2);

      const heirsFirstStorage = await this.main.heirs(user_2.address,0);
      const heirsSecondStorage = await this.main.heirs(user_2.address,1);
      const heirsThirdStorage = await this.main.heirs(user_2.address,2);

      const heirsStorage = [heirsFirstStorage, heirsSecondStorage, heirsThirdStorage];
      expect(heirs.toString()).eq(heirsStorage.toString());
    });

    it("should safe found and check ownerTimelock", async function () {
      const tokens = ["0xC3e9281b7E326ac1d675Ed02A6E95E1Ec21A057A","0xeDC255C351270682815b6960E6fdDc9955B7E355"];
      await this.main.connect(user_2).addTokensToSatelite(3, tokens);

      const heirs = ["0xd6328507f91C278EBff3C44Dc3D8F50274bC3a6D", "0x181931e6A6B8546f389c08BB53726EEEf638bb55", "0x635327F80C10A817CffCeb6633bd1781d3219Cc8"]

      await this.main.connect(user_2).secureFunds(heirs, 10, 2);

      const timelockStorage = await this.main.ownerTimelock(user_2.address);

      expect('10').eq(timelockStorage.toString());
    });

    it("should safe found and check votesRequired", async function () {
      const tokens = ["0xC3e9281b7E326ac1d675Ed02A6E95E1Ec21A057A","0xeDC255C351270682815b6960E6fdDc9955B7E355"];
      await this.main.connect(user_2).addTokensToSatelite(3, tokens);

      const heirs = ["0xd6328507f91C278EBff3C44Dc3D8F50274bC3a6D", "0x181931e6A6B8546f389c08BB53726EEEf638bb55", "0x635327F80C10A817CffCeb6633bd1781d3219Cc8"]

      await this.main.connect(user_2).secureFunds(heirs, 10, 2);

      const votesStorage = await this.main.votesRequired(user_2.address);

      expect('2').eq(votesStorage.toString());
    });

    it("should revert when there is no heirs", async function () {
      const tokens = ["0xC3e9281b7E326ac1d675Ed02A6E95E1Ec21A057A","0xeDC255C351270682815b6960E6fdDc9955B7E355"];
      await this.main.connect(user_2).addTokensToSatelite(3, tokens);

      const heirs = [];

      await expect(this.main.connect(user_2).secureFunds(heirs, 10, 2)).to.be.revertedWith("There is no heirs");
    });

    it("should resafe found and check update storage", async function () {
      const tokens = ["0xC3e9281b7E326ac1d675Ed02A6E95E1Ec21A057A","0xeDC255C351270682815b6960E6fdDc9955B7E355"];
      await this.main.connect(user_2).addTokensToSatelite(3, tokens);

      const firstHeirs = ["0xd6328507f91C278EBff3C44Dc3D8F50274bC3a6D", "0x181931e6A6B8546f389c08BB53726EEEf638bb55", "0x635327F80C10A817CffCeb6633bd1781d3219Cc8"]
      const secondHeirs = ["0x635327F80C10A817CffCeb6633bd1781d3219Cc8", "0x181931e6A6B8546f389c08BB53726EEEf638bb55"]

      await this.main.connect(user_2).secureFunds(firstHeirs, 10, 2);

      await this.main.connect(user_2).secureFunds(secondHeirs, 100, 1);

      const votesStorage = await this.main.votesRequired(user_2.address);
      const timelockStorage = await this.main.ownerTimelock(user_2.address);

      const heirsFirstStorage = await this.main.heirs(user_2.address,0);
      const heirsSecondStorage = await this.main.heirs(user_2.address,1);

      const heirsStorage = [heirsFirstStorage, heirsSecondStorage];

      expect('1').eq(votesStorage.toString());
      expect('100').eq(timelockStorage.toString());
      expect(secondHeirs.toString()).eq(heirsStorage.toString());
    });
  });

  describe("setVote()", function () {
    it("should safe found and check vote heir", async function () {
      const tokens = ["0xC3e9281b7E326ac1d675Ed02A6E95E1Ec21A057A","0xeDC255C351270682815b6960E6fdDc9955B7E355"];
      await this.main.connect(user_2).addTokensToSatelite(3, tokens);

      const heirs = [heir_1.address, heir_2.address, heir_3.address]

      await this.main.connect(user_2).secureFunds(heirs, 1000, 2);

      await this.main.connect(heir_1).setVote(user_2.address, true);

      const voteHeir1 = await this.main.heirVotes(heir_1.address, user_2.address);
      expect('true').eq(voteHeir1.toString());
    });

    it("should check there ir no update in timeWStarted", async function () {
      const tokens = ["0xC3e9281b7E326ac1d675Ed02A6E95E1Ec21A057A","0xeDC255C351270682815b6960E6fdDc9955B7E355"];
      await this.main.connect(user_2).addTokensToSatelite(3, tokens);

      const heirs = [heir_1.address, heir_2.address, heir_3.address]

      await this.main.connect(user_2).secureFunds(heirs, 1000, 2);

      await this.main.connect(heir_1).setVote(user_2.address, true);

      const timeStarted = await this.main.timeWStarted(user_2.address);
      expect('0').eq(timeStarted.toString());
    });

    it("should revert when try to vote with a non heir", async function () {
      const tokens = ["0xC3e9281b7E326ac1d675Ed02A6E95E1Ec21A057A","0xeDC255C351270682815b6960E6fdDc9955B7E355"];
      await this.main.connect(user_2).addTokensToSatelite(3, tokens);

      const heirs = [heir_1.address, heir_2.address, heir_3.address]

      await this.main.connect(user_2).secureFunds(heirs, 1000, 2);

      await expect(this.main.connect(user).setVote(user_2.address, true)).to.be.revertedWith("not an heir");
    });

    it("should check there is no update in timeWStarted with 2 votes to 3", async function () {
      const tokens = ["0xC3e9281b7E326ac1d675Ed02A6E95E1Ec21A057A","0xeDC255C351270682815b6960E6fdDc9955B7E355"];
      await this.main.connect(user_2).addTokensToSatelite(3, tokens);

      const heirs = [heir_1.address, heir_2.address, heir_3.address]

      await this.main.connect(user_2).secureFunds(heirs, 1000, 3);

      await this.main.connect(heir_1).setVote(user_2.address, true);
      await this.main.connect(heir_2).setVote(user_2.address, true);
      await this.main.connect(heir_3).setVote(user_2.address, false);

      const timeStarted = await this.main.timeWStarted(user_2.address);
      expect('0').eq(timeStarted.toString());
    });

    it("should check there is update in timeWStarted", async function () {
      const tokens = ["0xC3e9281b7E326ac1d675Ed02A6E95E1Ec21A057A","0xeDC255C351270682815b6960E6fdDc9955B7E355"];
      await this.main.connect(user_2).addTokensToSatelite(3, tokens);

      const heirs = [heir_1.address, heir_2.address, heir_3.address]

      await this.main.connect(user_2).secureFunds(heirs, 1000, 2);

      await this.main.connect(heir_1).setVote(user_2.address, true);
      await this.main.connect(heir_2).setVote(user_2.address, true);

      const timeStarted = await this.main.timeWStarted(user_2.address);
      expect('0').to.not.eq(timeStarted.toString());
    });
  });

  describe("veto()", function () {
    it("should start time and as owner excecute veto()", async function () {
      const tokens = ["0xC3e9281b7E326ac1d675Ed02A6E95E1Ec21A057A","0xeDC255C351270682815b6960E6fdDc9955B7E355"];
      await this.main.connect(user_2).addTokensToSatelite(3, tokens);

      const heirs = [heir_1.address, heir_2.address, heir_3.address]

      await this.main.connect(user_2).secureFunds(heirs, 1000, 2);

      await this.main.connect(heir_1).setVote(user_2.address, true);
      await this.main.connect(heir_2).setVote(user_2.address, true);
      await this.main.connect(heir_3).setVote(user_2.address, false);

      await this.main.connect(user_2).veto();
      const timeStarted = await this.main.timeWStarted(user_2.address);
      expect('0').eq(timeStarted.toString());
    });
  });


  describe("initializeWithdrawal()", function () {
    it("should revert when timeWStarted is zero for default value", async function () {
      const tokens = ["0xC3e9281b7E326ac1d675Ed02A6E95E1Ec21A057A","0xeDC255C351270682815b6960E6fdDc9955B7E355"];
      await this.main.connect(user_2).addTokensToSatelite(3, tokens);

      const heirs = [heir_1.address, heir_2.address, heir_3.address]

      await this.main.connect(user_2).secureFunds(heirs, 1000, 2);

      await expect(this.main.connect(heir_2).initializeWithdrawal(user_2.address)).to.be.revertedWith("withdraw not started");
    });

    it("should revert when timeWStarted is zero for veto", async function () {
      const tokens = ["0xC3e9281b7E326ac1d675Ed02A6E95E1Ec21A057A","0xeDC255C351270682815b6960E6fdDc9955B7E355"];
      await this.main.connect(user_2).addTokensToSatelite(3, tokens);

      const heirs = [heir_1.address, heir_2.address, heir_3.address]

      await this.main.connect(user_2).secureFunds(heirs, 1000, 2);

      await this.main.connect(heir_1).setVote(user_2.address, true);
      await this.main.connect(heir_2).setVote(user_2.address, true);
      await this.main.connect(heir_3).setVote(user_2.address, false);

      await this.main.connect(user_2).veto();

      await expect(this.main.connect(heir_2).initializeWithdrawal(user_2.address)).to.be.revertedWith("withdraw not started");
    });

    it("should revert when there is not yet", async function () {
      const tokens = ["0xC3e9281b7E326ac1d675Ed02A6E95E1Ec21A057A","0xeDC255C351270682815b6960E6fdDc9955B7E355"];
      await this.main.connect(user_2).addTokensToSatelite(3, tokens);

      const heirs = [heir_1.address, heir_2.address, heir_3.address]

      await this.main.connect(user_2).secureFunds(heirs, 1000, 2);

      await this.main.connect(heir_1).setVote(user_2.address, true);
      await this.main.connect(heir_2).setVote(user_2.address, true);
      await this.main.connect(heir_3).setVote(user_2.address, false);

      await expect(this.main.connect(heir_2).initializeWithdrawal(user_2.address)).to.be.revertedWith("not yet");
    });

    it("should revert when should pay for gas and doesnt send gas", async function () {
      const tokens = ["0xC3e9281b7E326ac1d675Ed02A6E95E1Ec21A057A","0xeDC255C351270682815b6960E6fdDc9955B7E355"];
      await this.main.connect(user_2).addTokensToSatelite(3, tokens);

      const heirs = [heir_1.address, heir_2.address, heir_3.address]

      await this.main.connect(user_2).secureFunds(heirs, 0, 2);

      await this.main.connect(heir_1).setVote(user_2.address, true);
      await this.main.connect(heir_2).setVote(user_2.address, true);
      await this.main.connect(heir_3).setVote(user_2.address, false);

      await expect(this.main.connect(heir_2).initializeWithdrawal(user_2.address)).to.be.revertedWith("should pay for gas");
    });

    it("should emit event when happen happy path", async function () {
      const tokens = ["0xC3e9281b7E326ac1d675Ed02A6E95E1Ec21A057A","0xeDC255C351270682815b6960E6fdDc9955B7E355"];
      await this.main.connect(user_2).addTokensToSatelite(3, tokens);

      const heirs = [heir_1.address, heir_2.address, heir_3.address]

      await this.main.connect(user_2).secureFunds(heirs, 0, 2);

      await this.main.connect(heir_1).setVote(user_2.address, true);
      await this.main.connect(heir_2).setVote(user_2.address, true);
      await this.main.connect(heir_3).setVote(user_2.address, false);

      await this.main.connect(heir_2).initializeWithdrawal(user_2.address, {value: ethers.utils.parseEther("1.0")});
    });
  });

});
