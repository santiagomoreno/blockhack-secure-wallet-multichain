// We require the Hardhat Runtime Environment explicitly here. This is optional
// but useful for running the script in a standalone fashion through `node <script>`.
//
// When running the script with `npx hardhat run <script>` you'll find the Hardhat
// Runtime Environment's members available in the global scope.
const hre = require("hardhat");

async function main() {



  //ropsten
  //const Main = await hre.ethers.getContractFactory("Main");
  //const main = await Main.deploy(0xBC6fcce7c5487d43830a219CA6E7B83238B41e71); //ropsten

  //await main.deployed();

  //console.log("main deployed to:", main.address);

  const Satelite = await hre.ethers.getContractFactory("Satelite");
  const satelite = await Satelite.deploy(0xBF62ef1486468a6bd26Dd669C06db43dEd5B849B);

  await satelite.deployed();

  console.log("satelite deployed to:", satelite.address);



}

// We recommend this pattern to be able to use async/await everywhere
// and properly handle errors.
main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });
